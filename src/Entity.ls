package
{
  import cocos2d.Cocos2D;
  import cocos2d.CCRect;
  import Loom.GameFramework.TickedComponent;
  import CocosDenshion.SimpleAudioEngine;

  /*
    Entity class will be a base class for all... entities to be based on
    It will be a component that holds all game logic info in it
    A Renderer should also be a component in game objects that have visuals
  */
  public class Entity extends TickedComponent
  {
    public var x:Number; // x co-ord
    public var y:Number; // y co-ord
    public var dy:Number = 0; // Only used for deaths
    public var facingRight:Boolean;
    public var spriteFrame:String;
    public var unitString:String; // Tell what texture to use
    public var frameNum:int = 1;
    public var framBounce:int = 1; // Determin which way the animation is looping
    public var dir:int = -1; // Temp direction var.
    public var speedMax:Number=2; // There is no accel at the moment, read as speed
    public var hitBox:CCRect; // Used for collisions
    public var health:int;
    public var alive:Boolean = true;

    public var walkingFrames:Vector.<String> =
    [
      "_walk1.png",
      "_stand.png",
      "_walk2.png"
    ];

    public var frameCD:Vector.<int> =
    [
      0,
      5
    ];

    // These next variable are for shooter, they should be in an extended entity
    // but there are only 5 hours left
    public var shootFrame:String = "_shoot2.png";
    public var shootCD:Vector.<int> =
    [
      0, // current
      120 // max
    ];



    // Contructor Yo
    public function Entity()
    {
      //hitbox.setRect(x,y,120,40);
    }

    public function onTick():void
    {
      x += dir * speedMax;

      if(dir!=0)
      {
        if(frameCD[0] >= frameCD[1])
        {
          changeFrame();
          frameCD[0]=0;
        }
        else
          frameCD[0]++;
      }
      /*if (unitString=="enemy")
      {
        shootTick();
        //Console.print("woop");
      }*/
    }

    // A rough hitbox, this needs HEAPS OF WORK!
    public function getHitBox(_fix:Number):CCRect
    {
      var newHitBox:CCRect;
      if (unitString == "turtle" || unitString == "turtle_mouth")
        newHitBox.setRect(x-hitBox.width/2+5,y-hitBox.height/2,hitBox.width,hitBox.height-5);
      else if(unitString == "man")
        newHitBox.setRect(x-hitBox.width/4,y-hitBox.height/2+15-_fix,hitBox.width/2,hitBox.height-15);
      else
        newHitBox.setRect(x-hitBox.width/4,y-hitBox.height/2 + 10,hitBox.width/2,hitBox.height-10);
      //Console.print(newHitBox.getMinY());
      return newHitBox;
    }

    // Changes the walking frame, called if the unit is walking
    public function changeFrame():void
    {
      spriteFrame=unitString+walkingFrames[frameNum];//"assets/sprites/"+

      if(frameNum == 0)
      {
        frameNum++;
        framBounce= 1;
      }
      else if(frameNum == walkingFrames.length-1)
      {
        frameNum--;
        framBounce=-1;
      }
      else
      {
        frameNum += framBounce;
      }
    }

    // For the shooter, should be moved
    public function shootTick(_game:Loomdumdare):void
    {
      if(shootCD[0]>shootCD[1])
      {
        shootCD[0] = 0;
        spriteFrame = unitString + shootFrame;
        x-=27; // So you can't run into the gun
        // shoot();
        _game.makeEnemy(x,"bullet");
        SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/laser.wav");
      }
      else
      {
        shootCD[0]++;
      }
      if(spriteFrame == unitString + shootFrame)
      {
        frameCD[0]++;
        if(frameCD[0]>=frameCD[1])
        {
          spriteFrame= unitString+walkingFrames[1];
          frameCD[0] = 0;
          x+=27;
        }

      }

    }
  }
}
