package
{
  import cocos2d.Cocos2D;
  import cocos2d.Cocos2DGame;
  import cocos2d.CCSprite;
  import cocos2d.ScaleMode;
  import cocos2d.CCTMXTiledMap;
  import cocos2d.CCPoint;
  import cocos2d.CCSpriteFrameCache;
  import cocos2d.CCRect;

  import UI.Label;

  import Loom.Platform.LoomKey;
  import Loom.GameFramework.LoomGameObject;
  import Loom.Application;

  import CocosDenshion.SimpleAudioEngine;

  import System.Platform.Gamepad;


  public class Loomdumdare extends Cocos2DGame
  {
    public var DEBUG = false;

    public var  bg:CCSprite;
    public var mess:Boolean = false;

    public var testSpawn:Boolean = false;
    // This list of public variables make me want to cry
    public var player:LoomGameObject; // The playable character

    public var enemies:Vector.<LoomGameObject> = []; // I guess I vector will do.

    public var level:int = 1; // The game level

    public var map:CCTMXTiledMap; // The tile map
    public var map2:CCTMXTiledMap; // The tile map

    public var sceneThing:SceneThing; // Determines what happens when the scene changes

    public var mapCurrent:CCPoint; // Where the bottom left currently is

    public var ground:int = 121; // The y co-ord of the ground
    public var music:int = 1;

    public var resetLoop = false; // for fun

    //public var buildings:CCSprite;
    public var endHead:CCSprite;

    public var pRef:Number = Cocos2D.getDisplayWidth(); // parrallax reference;

    public var gamepadConnected:Boolean = false;

    // The main game loop ticky stuff
    override public function onTick()
    {
      Gamepad.update();

      // I should move a lot of this into player, I'll refactor if I have time
      var core = getPlayerCore();
      if(core.x >= Cocos2D.getDisplayWidth()-50 && !resetLoop) //If on side of he map, move map.
      {
        //pRef = buildings.x;
        clearEnemies();
        if(map)
          getMap(level).setPositionX(getMap(level).getPositionX()-(Cocos2D.getDisplayWidth()));
        //map2.setPositionX(map2.getPositionX()-(Cocos2D.getDisplayWidth()));
        core.x = 64;
        sceneThing.changeScene(this, level);


      }
      if(core.x <= 40) //Player can't go left yo
      {
        core.x = 41;
      }

      collisions();
      shooters(); // This shouldn't exist but it needs fast implementation

      if(resetLoop) // If you die the world scrolls back to the start
      {
        if(getMap(level).getPositionX()>=0)
        {
          getMap(level).setPositionX(0);
          resetLoop = false;
          getPlayerCore().x = 60;
          getPlayerCore().y = ground;
          getPlayerCore().dy = 0;
        }
        else
        {
          getMap(level).setPositionX(getMap(level).getPositionX()+50);
        }

      }

      //buildings.x = pRef- getPlayerCore().x/8;
    }

    override public function run():void
    {
      // Comment out this line to turn off automatic scaling.
      //  layer.scaleMode = ScaleMode.FILL;

      super.run();

      CCSpriteFrameCache.sharedSpriteFrameCache().addSpriteFramesWithFile(
                "assets/sprites/ludumDareSprites.plist",
                "assets/sprites/ludumDareSprites.png");

      bg = CCSprite.createFromFile("assets/bg.png");
      bg.x = Cocos2D.getDisplayWidth()/ 2;
      bg.y = Cocos2D.getDisplayHeight() / 2;
      layer.addChild(bg);

      endHead = CCSprite.createFromFile("assets/endHead.png");
      endHead.x = -1000;
      endHead.y = 256;
      //endHead.scale = 0.5;
      layer.addChild(endHead);

      map = CCTMXTiledMap.create("assets/tiles/minimalism.tmx");
      layer.addChild(map);

      map2 = CCTMXTiledMap.create("assets/tiles/minimalism2.tmx");
      map2.setPositionY(2000); //Yes its horrible, I'm sorry
      layer.addChild(map2);

      sceneThing = new SceneThing;

      player = newPlayer();

      // Keyboard listener
      layer.setKeypadEnabled(true); // WE gone use it obv
      layer.onKeyDown += handleKeyDown;
      layer.onKeyUp += handleKeyUp;

      // Gamepad setup
      Gamepad.initialize();

      var pads:Vector.<Gamepad> = Gamepad.gamepads;
      for(var p=0; p<pads.length; p++)
      {
        var pad = pads[p];
        pad.buttonEvent += handleButton;
        pad.axisEvent += handleAxis;
        pad.hatEvent += handleHat;
      }
      if(pads.length > 0)
        gamepadConnected = true;


      // Audio preload
      SimpleAudioEngine.sharedEngine().preloadEffect("assets/sfx/jumpTemp.wav");
      SimpleAudioEngine.sharedEngine().preloadEffect("assets/sfx/death.wav");
      SimpleAudioEngine.sharedEngine().preloadEffect("assets/sfx/laser.wav");
      SimpleAudioEngine.sharedEngine().preloadEffect("assets/sfx/pickup.wav");
      SimpleAudioEngine.sharedEngine().preloadEffect("assets/sfx/mirror.wav");
      SimpleAudioEngine.sharedEngine().preloadEffect("assets/sfx/enDeath.wav");
      SimpleAudioEngine.sharedEngine().playBackgroundMusic("assets/music/music1.mp3");
      //SimpleAudioEngine.sharedEngine().preloadBackgroundMusic("assets/music/music2.mp3");


      // Cocos2D.setDisplayInfo(100,100,'apple'); // private =(



    }

    // This func decides what to do with key down events
    public function handleKeyDown(_key:int):void
    {
      var core = getPlayerCore();
      if(_key == LoomKey.W || _key == LoomKey.UP_ARROW)
        core.keys["jump"] = true;
      if(_key == LoomKey.A || _key == LoomKey.LEFT_ARROW)
        core.keys["left"] = true;
      if(_key == LoomKey.D || _key == LoomKey.RIGHT_ARROW)
        core.keys["right"] = true;
      if(_key == LoomKey.S || _key == LoomKey.DOWN_ARROW || _key == LoomKey.LEFT_CONTROL || _key == LoomKey.RIGHT_CONTROL)
        core.keys["crouch"] = true;
      if(_key == LoomKey.Q || _key == LoomKey.LEFT_SHIFT || _key == LoomKey.RIGHT_SHIFT)
        core.keys["shield"] = true;
      if(_key == LoomKey.SPACEBAR)
        core.keys["jump"] = true;
      if(_key == LoomKey.P && !testSpawn)
      {
        testSpawn = !testSpawn;
        makeEnemy(800,"enemy");
      }

    }

    // This func decides what to do with key up events
    public function handleKeyUp(_key:int):void
    {
      var core = getPlayerCore();
      if(_key == LoomKey.W || _key == LoomKey.UP_ARROW)
        core.keys["jump"] = false;
      if(_key == LoomKey.A || _key == LoomKey.LEFT_ARROW)
        core.keys["left"] = false;
      if(_key == LoomKey.D || _key == LoomKey.RIGHT_ARROW)
        core.keys["right"] = false;
      if(_key == LoomKey.S || _key == LoomKey.DOWN_ARROW || _key == LoomKey.LEFT_CONTROL || _key == LoomKey.RIGHT_CONTROL)
        core.keys["crouch"] = false;
      if(_key == LoomKey.Q || _key == LoomKey.LEFT_SHIFT || _key == LoomKey.RIGHT_SHIFT)
        core.keys["shield"] = false;
      if(_key == LoomKey.SPACEBAR)
        core.keys["jump"] = false;
      if(_key == LoomKey.P && testSpawn)
      {
        testSpawn = !testSpawn;
      }
    }

    public function handleHat(hat:int, state:int):void
    {

    }

    public function handleAxis(axis:int, state:float):void
    {
      var core = getPlayerCore();
      if(axis==0)
      {
        if(state>0.25)
        {
          core.keys["right"] = true;
        }
        else if(state<-0.25)
        {
          core.keys["left"] = true;
        }
        else
        {
          core.keys["right"] = false;
          core.keys["left"] = false;
        }
      }
    }

    public function handleButton(button:int, state:Boolean):void
    {
      var core = getPlayerCore();
      if(button == 0)
      {
        core.keys["jump"] = state;
      }
      if(button == 2)
      {
        core.keys["crouch"] = state;
      }
      if(button == 3)
      {
        core.keys["shield"] = state;
      }
      if(button == 4 && DEBUG)
      {
        if(state)
        {
          clearEnemies;
          sceneThing.sceneNumber++;
          Console.print(sceneThing.sceneNumber);
        }
      }
      if(button == 5 && DEBUG)
      {
        if(state)
        {
          core.speedMax = 10;
        }
        else
        {
          core.speedMax = 3;
        }
      }

    }

    public function getRenderer(_lgo:LoomGameObject):EntityRenderer
    {
      return _lgo.lookupComponentByName("renderer") as EntityRenderer;
    }

    public function newPlayer():LoomGameObject // SHould be changed to more general function
    {
      var lgo = new LoomGameObject();
      lgo.owningGroup = group;

      var playerCore = new Player();
      var renderer = new EntityRenderer(layer, "man_stand.png"); //assets/sprites/

      playerCore.unitString = "man";
      playerCore.hitBox = CCSpriteFrameCache.sharedSpriteFrameCache().spriteFrameByName("man_stand.png").getRect();
      playerCore.standHeight = playerCore.hitBox.height;
      playerCore.crouchHeight = playerCore.hitBox.height/2;
      // Set variables and bind them to renderer
      playerCore.x = 60;
      renderer.addBinding("x", "@core.x");



      playerCore.y = ground;
      renderer.addBinding("y", "@core.y");

      playerCore.spriteFrame = "man_stand.png"; //assets/sprites/
      renderer.addBinding("textureFile","@core.spriteFrame");

      playerCore.facingRight = false;
      renderer.addBinding("facing","@core.facingRight")

      lgo.addComponent(playerCore, "core");
      lgo.addComponent(renderer, "renderer");

      lgo.initialize();

      return lgo;
    }

    public function getPlayerCore():Player
    {
      return player.lookupComponentByName("core") as Player;
    }

    public function newEnemy(_x:Number,_type:String):LoomGameObject //Should probably just make newEntity()
    {
      var lgo = new LoomGameObject();
      lgo.owningGroup = group;

      var entityCore = new Entity();
      var renderer = new EntityRenderer(layer, _type+"_stand.png");//"assets/sprites/"+

      entityCore.unitString=_type;
      entityCore.hitBox = CCSpriteFrameCache.sharedSpriteFrameCache().spriteFrameByName(_type+"_stand.png").getRect();
      // Set variables and bind them to renderer
      entityCore.x = _x;
      renderer.addBinding("x", "@core.x");

      entityCore.y = ground;
      renderer.addBinding("y", "@core.y");
      // Switch would make more sense here, NO TIME
      setTypeStats(_type,entityCore);

      entityCore.spriteFrame = _type+"_stand.png";//"assets/sprites/"+
      renderer.addBinding("textureFile","@core.spriteFrame");

      entityCore.facingRight = false;
      renderer.addBinding("facing","@core.facingRight")

      lgo.addComponent(entityCore, "core");
      lgo.addComponent(renderer, "renderer");

      lgo.initialize();

      return lgo;
    }
    public function getEntityCore(_lgo:LoomGameObject):Entity
    {
      return _lgo.lookupComponentByName("core") as Entity;
    }

    public function reuseEnemy(_entityCore:Entity,_x:Number,_type:String):void
    {
      _entityCore.x = _x;
      _entityCore.dir = -1;
      _entityCore.alive = true;
      _entityCore.unitString=_type;
      _entityCore.hitBox = CCSpriteFrameCache.sharedSpriteFrameCache().spriteFrameByName(_type+"_stand.png").getRect();
      _entityCore.y = ground;
      _entityCore.spriteFrame = _type+"_stand.png";
      _entityCore.facingRight = false;
      setTypeStats(_type,_entityCore);

    }

    public function setTypeStats(__type:String, _entityCore:Entity):void
    {
      if( __type=="turtle"|| __type=="turtle_mouth" )
      {
        _entityCore.y -= 32;
        _entityCore.speedMax = 2;
      }
      else if(__type=="bird")
      {
        _entityCore.y += 58;
        _entityCore.speedMax = 6;
        _entityCore.frameCD[1]=3;
      }
      else if(__type=="mirror_shield")
      {
        _entityCore.y -= 42;
        _entityCore.speedMax = 0;
      }
      else if(__type=="enemy")
      {
        _entityCore.dir = 0;
        _entityCore.speedMax = 0;
        _entityCore.frameCD[1] = 30;
        _entityCore.shootCD[0] = 90;
      }
      else if(__type == "end")
      {
        _entityCore.dir = 0;
        _entityCore.speedMax = 0;

      }
      else if(__type == "bullet")
      {
        //entityCore.dir = 0;
        _entityCore.y +=10;
        _entityCore.speedMax = 8;
      }
    }
    // Function to be called to make an enemy and add it to enemies
    public function makeEnemy(_x:Number,_type:String)
    {
      var made:Boolean = false; // Hack cause I can't destroy things for some reason
      for(var e:int = 0; e<enemies.length; e++)
      {
        if(!getEntityCore(enemies[e]).alive)
        {
          reuseEnemy(getEntityCore(enemies[e]),_x,_type);
          made = true;
          break;
        }
      }
      if(!made)
      {
        enemies.pushSingle(newEnemy(_x,_type));
      }
    }

    public function changeTrack(_trackNo:int):void
    {
      SimpleAudioEngine.sharedEngine().stopBackgroundMusic(true);
      SimpleAudioEngine.sharedEngine().playBackgroundMusic("assets/music/music"+_trackNo+".mp3");
      music = _trackNo;
    }
    // Test for collisions and collide if need be
    public function collisions():void
    {
      var pTemp = getPlayerCore();
      var crouchFix:Number = 0;
      if(pTemp.isCrouching)
      {
        pTemp.hitBox.height = pTemp.crouchHeight;
        crouchFix = 10;
      }
      else
        pTemp.hitBox.height = pTemp.standHeight;
      for(var i:int = 0; i<enemies.length; i++)
      {
        var eTemp = getEntityCore(enemies[i]);
        if(eTemp.x<-100)
        {
          eTemp.alive = false;
        }
        if(onScreen(eTemp.x) && eTemp.getHitBox(0).CCRectIntersectsRect(eTemp.getHitBox(0),pTemp.getHitBox(crouchFix)))
        {
          //Console.print(pTemp.getHitBox().getMinX() + " " eTemp.getHitBox().getMinX())
          //pTemp.x = 60;
          //reset();
          //Console.print(eTemp.x);
          // Bounce off turtle
          if(eTemp.unitString == "turtle" && pTemp.isJumping)
          {
            pTemp.bounce();
          }
          else if(eTemp.unitString == "end")
          {
            endLevel();
          }
          else if(eTemp.unitString == "bullet" && pTemp.isShielding)
          {
            eTemp.dir = 1;
            eTemp.facingRight = true;
            SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/mirror.wav");
          }
          else if(eTemp.unitString == "mirror_shield")
          {
            SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/pickup.wav");
            eTemp.x = -1000;
            pTemp.hasShield = true;
          }
          else
          {
            SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/death.wav");
            if(!DEBUG)
            {
              pTemp.dy=20;
              reset();
            }
          }
        }
        if(eTemp.unitString=="bullet" && eTemp.x > Cocos2D.getDisplayWidth())
        {
            eTemp.x = -1000;
            eTemp.y = -500;
            eTemp.dir = 0;
        }
        if(onScreen(eTemp.x) && eTemp.unitString=="bullet" && eTemp.dir==1)
        {

          for(var j:int = 0; j<enemies.length; j++)
          {
            var eTemp2 = getEntityCore(enemies[j]);
            if(eTemp2.unitString == "enemy")
            {
              if(eTemp.getHitBox(0).CCRectIntersectsRect(eTemp.getHitBox(0),eTemp2.getHitBox(0)))
              {
                eTemp2.x =-100;
                eTemp.x = -1000;
                eTemp.y = -500;
                eTemp.dir = 0;
                Console.print("Killed him");
                SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/enDeath.wav");
              }
            }
          }
        }

      }
    }

    // This is the worst hack in the world, having a problem destroying lgos.
    public function clearEnemies():void
    {
      for(var i:int = 0; i<enemies.length; i++)
      {
        getEntityCore(enemies[i]).x = -100;
      }
    }
    public function reset():void
    {
      changeTrack(1);
      resetLoop = true;
      clearEnemies();
      sceneThing.sceneNumber=0;
      //map.setPositionX(0);
      //getPlayerCore().x = 60;


      //enemies[0].destroy();

    }

    // Determines if a new level is created or
    // the game is won.
    public function endLevel():void
    {
      if(level==1)
      {
        level++;
        reset();
        map.setPositionY(2000);
        map2.setPositionY(0);
        sceneThing.sceneNumber = 0;
        endHead.x = -1000;
        makeEnemy(400,"mirror_shield");
        bg.y-=100;
        //makeNewLevel(level);
      }
      else
      {
        displayVictory();
      }
    }

    // Loads a new map
    /*public function makeNewLevel(_level:int):void//CCTMXTiledMap
    {
      //var newMap = CCTMXTiledMap.create("assets/tiles/minimalism"+(_level+1)+".tmx");
      //map = newMap;
      //map.removeFromParentAndCleanup();
      //map = newMap;
      //var mapN = CCTMXTiledMap.create("assets/tiles/minimalism"+(_level+1)+".tmx");
      //return mapN;
    }*/

    // Hack because I don't have time to
    // implement new maps properly
    public function getMap(_mapID:int):CCTMXTiledMap
    {
      if(_mapID==1)
      {
        return map;
      }
      else
      {
        return map2;
      }
    }

    // When the game is over
    public function displayVictory():void
    {
      if(!mess)
      {
        var spr = CCSprite.createFromFile("assets/victoryOverlay.png");
        spr.x = Cocos2D.getDisplayWidth()/2;
        spr.y = Cocos2D.getDisplayHeight()/2;
        layer.addChild(spr);
        mess=true;
      }
    }

    public function shooters():void
    {
      for(var i:int = 0; i<enemies.length; i++)
      {
        var eTemp = getEntityCore(enemies[i]);
        if(eTemp.unitString=="enemy" && onScreen(eTemp.x))
        {
          eTemp.shootTick(this);
        }
      }
    }

    public function onScreen(_x:Number):Boolean
    {
      if(_x>0 && _x<Cocos2D.getDisplayWidth())
        return true;
      else
        return false;
    }
  }
}
