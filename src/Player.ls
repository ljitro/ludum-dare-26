package
{
  import Entity;
  import CocosDenshion.SimpleAudioEngine;

  /*
    This class holds all game logic variable for the player
  */
  public class Player extends Entity
  {
    public var jump:Dictionary.<String, Number> =
    {
      "vC":0,
      "v0":4,
      "a":-0.2
    }

    public var keys:Dictionary.<String, Boolean> =
    {
      "left" : false,
      "right" : false,
      "jump" : false,
      "crouch" : false,
      "shield" : false
    };

    public var hasShield:Boolean = false;
    public var isJumping:Boolean = false;
    public var isCrouching:Boolean = false;
    public var isShielding:Boolean = false;
    public var ground = 121;
    public var crouchHeight:Number;
    public var standHeight:Number;

    /*override public function getHitBox():CCRect
    {
      var newHitBox:CCRect;
      newHitBox.setRect(x-hitBox.width/2,y-hitBox.height/2,hitBox.width-10,hitBox.height);
      //Console.print(newHitBox.getMinY());
      return newHitBox;
    }*/

    public function Player()
    {
      speedMax = 3;
    }

    // Additional bounce for enemies
    // plan to make it more complicated
    public function bounce():void
    {
      jump["vC"] = jump["v0"];
      SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/jumpTemp.wav");
    }
    override public function onTick():void
    {
      x += dir * speedMax;
      y-=dy;
      if(keys["left"])
      {
        if(!isJumping)
          dir = -1;
        facingRight=true;
      }
      else if(keys["right"])
      {
        if(!isJumping)
          dir = 1;
        facingRight=false;
      }
      else if (!isJumping)
        dir = 0;
      if(keys["crouch"])
      {
        dir = 0;
        isCrouching=true;
      }
      else
        isCrouching=false;
      if(keys["shield"] && hasShield)
      {
        dir = 0;
        isShielding=true;
        isCrouching=false;
      }
      else
      {
        isShielding=false;
      }

      if((y == ground) && keys["jump"])
      {
        jump["vC"] = jump["v0"];
        isJumping = true;
        SimpleAudioEngine.sharedEngine().playEffect("assets/sfx/jumpTemp.wav"); //play sound
        keys["jump"] = false; //So the player has to press the jump key again.
        //Console.print(jumpVel);
      }
      if(isJumping)
      {
        if(jump["vC"] < 0 && y < ground)
        {
          isJumping = false;
          y = ground;
          //getHitbox();
          //break;
        }
        else
        {
          y += jump["vC"];
          jump["vC"] += jump["a"];
        }
      }

      if(dir!=0)
      {
        if(frameCD[0] >= frameCD[1])
        {
          changeFrame();
          frameCD[0]=0;
        }
        else
          frameCD[0]++;
      }
      else
      {
        spriteFrame = "man_stand.png";//assets/sprites/
        //sprite.setTextureFile("assets/sprites/man_stand.png");
      }
      if(isCrouching)
      {
        spriteFrame = unitString + "_crouch.png";//assets/sprites/
      }
      if(isShielding)
      {
        spriteFrame = unitString + "_mirror.png";
      }
    }

  }
}
