package
{
  //This is going to be a class that has a specific function for the scene it is on.
  public class SceneThing
  {
    public var sceneNumber:int = 0; // The scene the game is on

    public function SceneThing():void
    {

    }
    // This should be done in files but whatever
    public function changeScene(_game:Loomdumdare, _level:int)
    {
      sceneNumber++;
      if(_level == 1) // So sorry
      {
        if(sceneNumber==1)
        {
          _game.makeEnemy(500,"turtle");
          _game.makeEnemy(1800,"bird");
        }
        if(sceneNumber==2)
        {
          _game.changeTrack(2);
          _game.makeEnemy(400,"turtle");
          _game.makeEnemy(600,"bird");
          _game.makeEnemy(650,"turtle_mouth");
          _game.makeEnemy(1000,"turtle_mouth");
          //_game.makeEnemy(800,"turtle");
        }
        if(sceneNumber==3)
        {
          _game.makeEnemy(400,"turtle");
          _game.makeEnemy(600,"bird");
          _game.makeEnemy(600,"turtle_mouth");
          _game.makeEnemy(810,"turtle_mouth");
          _game.makeEnemy(910,"turtle");
          _game.makeEnemy(1010,"turtle_mouth");
          //_game.makeEnemy(800,"turtle");
        }
        if(sceneNumber==4)
        {
          _game.makeEnemy(400,"turtle");
          //_game.makeEnemy(600,"bird");
          _game.makeEnemy(440,"turtle_mouth");
          //_game.makeEnemy(540,"turtle_mouth");
          _game.makeEnemy(640,"turtle_mouth");
          _game.makeEnemy(810,"turtle_mouth");
          _game.makeEnemy(1900,"bird");
          //_game.makeEnemy(810,"turtle_mouth");
          //_game.makeEnemy(800,"turtle");
        }
        if(sceneNumber==5)
        {
          _game.makeEnemy(600,"bird");
         // _game.makeEnemy(300,"turtle");
          _game.makeEnemy(1000,"bird");
          _game.makeEnemy(500,"turtle");
          _game.makeEnemy(1500,"bird");
          _game.makeEnemy(750,"turtle");
          //_game.makeEnemy(1900,"bird");
          //_game.makeEnemy(810,"turtle_mouth");
          //_game.makeEnemy(800,"turtle");
        }
        if(sceneNumber==6)
        {
          _game.makeEnemy(256,"end");
          _game.endHead.x=512;
        }
      }
      else if (_level==2)
      {
        if(sceneNumber==1)
        {
          _game.makeEnemy(800,"enemy");
        }
        if(sceneNumber==2)
        {
          _game.changeTrack(2);
          _game.makeEnemy(400,"turtle");
          _game.makeEnemy(600,"bird");
          _game.makeEnemy(900,"enemy");
          //_game.makeEnemy(800,"turtle");
        }
        if(sceneNumber==3)
        {
          _game.makeEnemy(400,"turtle");
          _game.makeEnemy(600,"bird");
          _game.makeEnemy(600,"turtle_mouth");
          _game.makeEnemy(810,"turtle");
          _game.makeEnemy(910,"turtle");
          _game.makeEnemy(1010,"turtle");
          _game.makeEnemy(900,"enemy");
          //_game.makeEnemy(800,"turtle");
        }
        if(sceneNumber==4)
        {
          //_game.makeEnemy(810,"turtle_mouth");
          //_game.makeEnemy(800,"turtle");
          _game.makeEnemy(400,"turtle");
          _game.makeEnemy(600,"turtle_mouth");
          _game.makeEnemy(810,"turtle");
          _game.makeEnemy(910,"turtle_mouth");
          _game.makeEnemy(1010,"turtle");
          _game.makeEnemy(900,"enemy");
        }
        if(sceneNumber==5)
        {
          _game.makeEnemy(400,"bird");
          _game.makeEnemy(600,"bird");
          _game.makeEnemy(800,"bird");
          _game.makeEnemy(1000,"bird");
         // _game.makeEnemy(600,"bird");
          //_game.makeEnemy(600,"bird");
          _game.makeEnemy(1400,"bird");
          _game.makeEnemy(1600,"bird");
          _game.makeEnemy(1800,"bird");
          _game.makeEnemy(2000,"bird");

          _game.makeEnemy(900,"enemy");
        }
        if(sceneNumber==6)
        {
          _game.makeEnemy(256,"end");
          _game.endHead.x=512;
        }
      }
    }
  }
}
