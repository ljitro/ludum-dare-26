package
{
  import cocos2d.CCSprite;
  import cocos2d.CCSpriteFrame;
  import cocos2d.CCSpriteFrameCache;
  import cocos2d.CCNode;
  import Loom.GameFramework.AnimatedComponent;

  public class EntityRenderer extends AnimatedComponent
  {
    var frame:CCSpriteFrame;
    public var sprite:CCSprite;
    var texture:String;
    var parent:CCNode;

    public function EntityRenderer(_parent:CCNode, _texture:String)
    {
      parent = _parent;
      texture = _texture;

      frame = CCSpriteFrameCache.sharedSpriteFrameCache().spriteFrameByName(texture);
      sprite = CCSprite.createWithSpriteFrame(frame);
    }

    public function set x(value:Number):void
    {
      if(sprite)
        sprite.setPositionX(value);
    }

    public function set y(value:Number):void
    {
      if(sprite)
        sprite.setPositionY(value);
    }

    public function set scale(value:Number):void
    {
      if(sprite)
        sprite.setScale(value);
    }

    public function set rotation(value:Number):void
    {
      if(sprite)
        sprite.setRotation(value);
    }

    public function set facing(value:Boolean):void
    {
      if(sprite)
        sprite.setFlipX(value);
    }

    public function set textureFile(value:String):void
    {
      // This probably shouldnt happen every frame, but there wont be too many units
      var Frame2 = CCSpriteFrameCache.sharedSpriteFrameCache().spriteFrameByName(value);
      if(Frame2)
      {
        sprite.setDisplayFrame(Frame2);
      }


    }
    /*public function set textureFile(value:String):void
    {
      if(sprite)
        sprite.setTextureFile(value);
    }*/

    protected function onAdd():Boolean
    {
      if(!super.onAdd())
        return false;

      parent.addChild(sprite);

      return true;
    }

    protected function onRemove():void
    {
      parent.removeChild(sprite);

      super.onRemove();
    }
  }
}
